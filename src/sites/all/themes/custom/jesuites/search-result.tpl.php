<?php

/**
 * @file
 * Default theme implementation for displaying a single search result.
 *
 * This template renders a single search result and is collected into
 * search-results.tpl.php. This and the parent template are
 * dependent to one another sharing the markup for definition lists.
 *
 * Available variables:
 * - $url: URL of the result.
 * - $title: Title of the result.
 * - $snippet: A small preview of the result. Does not apply to user searches.
 * - $info: String of all the meta information ready for print. Does not apply
 *   to user searches.
 * - $info_split: Contains same data as $info, split into a keyed array.
 * - $module: The machine-readable name of the module (tab) being searched, such
 *   as "node" or "user".
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Default keys within $info_split:
 * - $info_split['type']: Node type (or item type string supplied by module).
 * - $info_split['user']: Author of the node linked to users profile. Depends
 *   on permission.
 * - $info_split['date']: Last update of the node. Short formatted.
 * - $info_split['comment']: Number of comments output as "% comments", %
 *   being the count. Depends on comment.module.
 *
 * Other variables:
 * - $classes_array: Array of HTML class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $title_attributes_array: Array of HTML attributes for the title. It is
 *   flattened into a string within the variable $title_attributes.
 * - $content_attributes_array: Array of HTML attributes for the content. It is
 *   flattened into a string within the variable $content_attributes.
 *
 * Since $info_split is keyed, a direct print of the item is possible.
 * This array does not apply to user searches so it is recommended to check
 * for its existence before printing. The default keys of 'type', 'user' and
 * 'date' always exist for node searches. Modules may provide other data.
 * @code
 *   <?php if (isset($info_split['comment'])): ?>
 *     <span class="info-comment">
 *       <?php print $info_split['comment']; ?>
 *     </span>
 *   <?php endif; ?>
 * @endcode
 *
 * To check for all available data within $info_split, use the code below.
 * @code
 *   <?php print '<pre>'. check_plain(print_r($info_split, 1)) .'</pre>'; ?>
 * @endcode
 *
 * @see template_preprocess()
 * @see template_preprocess_search_result()
 * @see template_process()
 */

global $base_path;
global $language;

//echo "<br>LANGUAGE:".$result['node']->language." --- GLOBAL:".$language->language." --- TITLE:".$result['node']->title."<br>";


// MOSTREM NOM�S RESULTATS DE LA CERCA QUE SIGUIN DE L'IDIOMA ACTUAL

if ($result['node']->language == $language->language) {			?>

		<li class="<?php print $classes; ?>"<?php print $attributes; ?>>

		  <?php print render($title_prefix);


		  		$titulo = $title; 
				$destino = $url;
		
				// SI EL RESULTAT �S UN TEXTE B�SIC (DESPLEGABLE), BUSQUEM I MOSTREM LA P�GINA ON APAREIX EL DESPLEGABLE
				if ($result['node']->type == "texte_basic") {
		
					$node = $result['node'];
		
					// LLEGIM LA TAXONOMIA A LA QUE PERTANY
					$terms = field_view_field('node', $node, 'field_tipus_contingut');
					$term_id = $terms['#items'][0]['tid'];

					// LLEGIM EL T�TOL DE LA TAXONOMIA, I L'AFEGIM AL DAVANT DEL T�TOL
					$term = taxonomy_term_load($term_id);
					$term_title = i18n_taxonomy_term_name($term, $language->language);

					$titulo =  $term_title . " - " . $titulo;

					// MUNTEM LA URL DEST�, QUE SER� L'IDIOMA ACTUAL, I LA DESCRIPCI� DE LA TAXONOMIA
					$term_url = $term->description;
					$lang_url = $language->prefix == "" ? "" : $language->prefix . "/";

					$destino = $base_path . $lang_url . $term_url;
					
				}
		
		  ?>
		
		  <h3 class="title"<?php print $title_attributes; ?>>
			<a href="<?php print $destino; ?>"><?php print $titulo; ?> <span class="blau">(<?php echo node_tipus($result['node']); ?>)</span></a>
		  </h3>
		  <?php print render($title_suffix); ?>
		  <div class="search-snippet-info">
			<?php if ($snippet): ?>
			  <p class="search-snippet"<?php print $content_attributes; ?>><?php print $snippet; ?></p>
			<?php endif; ?>
		  </div>
		</li>

<?php
}
?>