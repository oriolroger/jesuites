<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<?php //print_r($row);

 ?>
<?php foreach ($fields as $id => $field): ?>
  <?php if (!empty($field->separator)): ?>
    <?php print $field->separator; ?>
  <?php endif; ?>

<?php

//     if ( $field->label == " / ") {
//              if ($field->class == "field-rating-artistica-final") {
//                echo "DENTRO";
//                $title = $view->result->{$view->field['title']->field_alias}; 
//                $data = $view->field['field_rating_artistica-value']->get_value(0);
//					$data = $row->field_field_interes_2[0]['raw']['value'];

//                $data = $view->field['views-field-field-interes'];
//				print_r($row);
//				print $row->{$field->field_interes}[und][0][value];
//                echo "VALOR:".$data;
//                $field->label == "";
//            }
 
/*              if ($field->class == "field-rating-fisica-final" && $field->content == 0)         $field->label == "";
              if ($field->class == "field-rating-tecnica-final" && $field->content == 0)         $field->label == "";
*/
 


?>

  <?php print $field->wrapper_prefix; ?>
    <?php print $field->label_html; ?>
    <?php print $field->content; ?>
  <?php print $field->wrapper_suffix; ?>
<?php endforeach; ?>
