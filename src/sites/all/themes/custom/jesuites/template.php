<?php
/** 
 * @file
 * Contains theme override functions and preprocess functions for the theme.
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. You can modify or override Drupal's theme
 *   functions, intercept or make additional variables available to your theme,
 *   and create custom PHP logic. For more information, please visit the Theme
 *   Developer's Guide on Drupal.org: http://drupal.org/theme-guide
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   The Drupal theme system uses special theme functions to generate HTML
 *   output automatically. Often we wish to customize this HTML output. To do
 *   this, we have to override the theme function. You have to first find the
 *   theme function that generates the output, and then "catch" it and modify it
 *   here. The easiest way to do it is to copy the original function in its
 *   entirety and paste it here, changing the prefix from theme_ to jesuites_.
 *   For example:
 *
 *     original: theme_breadcrumb()
 *     theme override: jesuites_breadcrumb()
 *
 *   where jesuites is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_breadcrumb() function.
 *
 *   If you would like to override either of the two theme functions used in Zen
 *   core, you should first look at how Zen core implements those functions:
 *     theme_breadcrumbs()      in zen/template.php
 *     theme_menu_local_tasks() in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold various pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called template suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node-forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions and template suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440
 *   and http://drupal.org/node/190815#template-suggestions
 */

/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
/* -- Delete this line if you want to use this function
function jesuites_preprocess_html(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // The body tag's classes are controlled by the $classes_array variable. To
  // remove a class from $classes_array, use array_diff().
  //$variables['classes_array'] = array_diff($variables['classes_array'], array('class-to-remove'));
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */

function jesuites_preprocess_page(&$variables, $hook) {

	if (!empty($variables['theme_hook_suggestions'])) { 

		// SI LA P�GINA ACTUAL �S UNA TAXONOMIA, MOSTREM ACTIVAT EL SEU MEN� CORRESPONENT (PER EXEMPLE, TAGS --> SALA DE PREMSA)

		if ($variables['theme_hook_suggestions'][0] == "page__taxonomy") {
			$term = taxonomy_term_load(arg(2));
			$term_name = $term->vocabulary_machine_name;

			// Array que relaciona cada nom de Taxonomia amb el seu tipus de contingut
			$nodes = array (
				"tags" => "nota_de_premsa"
			);

			// Recorrem el vector de taxonomies, fins que trobem l'actual
			foreach ($nodes as $termino => $tipo) {
				if ($termino == $term_name) {

					// Mitjan�ant jQuery, mostrem Activat el men� de l'esquerra
					drupal_add_js("	
						jQuery(document).ready(function () { 
							jQuery('#block-menu-menu-esquerra ul.menu li#" . $tipo . " a').addClass('active');
						}
					);", "inline");	

					break;
				}
			}
		}
	}



/*
	$node = node_load(arg(1));
	if ($node) {
		$terms = field_view_field('node', $node, 'field_tipus_contingut');
		print_r($terms);
		$term_name =   $terms['#items'][0]['name'];
		return ($term_name);
	}
	else { return; }
*/
//	echo "ARG: ".arg(0)." - " . arg(1) . " - ".arg(2);


// MOSTREM EL NOM DEL VOCABULARI AL QUAL PERTANY UN TERM ID
/*
	$term = taxonomy_term_load(arg(2));
	print_r($term);
	echo "NAME:".$term->vocabulary_machine_name;
*/

/*
	while ($parents = taxonomy_get_parents(arg(2))) {
		$current = array_shift($parents);
		echo "TAX: " . $current->name . " --- " . $current->tid;
	}
*/
	//$match |= ((arg(0) == 'taxonomy') && (arg(1) == 'term'));


	//	print_r($term_name);
	/*
	  foreach($node->taxonomy as $term) {
		$terms[] = $term->tid;
	  }
	  return implode('+',$terms);
	}
	else { return; }
	*/
	
	/*
		$active_trail = menu_get_active_trail();
		end($active_trail);
		$parent = pos($active_trail);
	//	print_r($parent[mlid]);
	//	print_r($parent);
		if (!is_null($parent)) {
			if ($parent['menu_name'] == "main-menu") {
				print_r($active_trail);
			}
		}
	*/
	
	
	
	
	/*
	$active_trail = menu_get_active_trail();
	end($active_trail);
	$actual = pos($active_trail);
	if (!is_null($actual)) {
	  if ($actual['menu_name'] == "main-menu") {
		$parent = prev($active_trail);
		if (!is_null($parent) && $parent['type'] == 0) {
	//      return($actual['mlid']);
		  echo "ACTUAL:".$actual['mlid'];
		}
	  }
	}
	*/
	
	
	/*
		$active_trail = menu_get_active_trail();
		end($active_trail);
		$actual = pos($active_trail);
		if (!is_null($actual)) {
		  if ($actual['menu_name'] == "main-menu") {
			$parent = prev($active_trail);
			if ($parent['type'] != 0) {
				print_r($parent['mlid']);
	//			print_r($active_trail);
			}
		  }
		}
	*/
	
	
	/*	$xxx = menu_get_menu();
		$node = menu_get_item();
		$menu_item = !empty($node->menu_node_links) ? array_shift($node->menu_node_links) : NULL;
		$mlid = !is_null($menu_item) ? $menu_item->mlid : NULL;
		print_r($node);
	
		print_r($variables['page']);
	*/
	
	//	$args = arg();
	//	print_r($args);
	//	$node = node_load($args[1]);




}




/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/* -- Delete this line if you want to use this function	*/
function jesuites_preprocess_node(&$variables, $hook) {

	if (!empty($variables['node'])) {

		$node = $variables['node'];

		// Si estem dins una Not�cia, Agenda, Nota de Premsa, Publicaci�..., mostrem Activat el men� de l'esquerra 

		$nodes = array ("noticia", "agenda", "nota_de_premsa");

		if (in_array($node->type, $nodes)) {


			// Tradu�m el text fixe del BACK BUTTON, perqu� no funciona la traducci� autom�ticament
			$translate_back_button = t($node->field_back_button['und'][0]['title']);


			drupal_add_js("	
				jQuery(document).ready(function () { 

					// MOSTREM ACTIVAT EL MEN� DE L'ESQUERRA (DE COLOR TARONJA)
					jQuery('#block-menu-menu-esquerra ul.menu li#" . $node->type . " a').addClass('active');

					// TRADU�M EL TEXT FIXE DEL BACK BUTTON, PERQU� NO FUNCIONA LA TRADUCCI� AUTOM�TICAMENT
					jQuery('div.field-name-field-back-button a').html('" . $translate_back_button. "');

				}
			);", "inline");
		}



		// Si hi ha arxius adjunts, afegim el "target=_blank" perqu� s'obrin en una altra pestanya
		drupal_add_js("	
			jQuery(document).ready(function () {
				field = jQuery('#main .arxius-adjunts a');
				field.attr('target','_blank');
			}
		);", "inline");


		// Si es mostren els links a xarxes socials, amaguem el petit de Twitter (nom�s es far� servir per m�bils)
		if (module_exists('service_links')) {

			drupal_add_js("	
				jQuery(document).ready(function () {
					jQuery('ul li.service-links-twitter').remove();
				}
			);", "inline");

		}

	}


}
// 

/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function jesuites_preprocess_comment(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function jesuites_preprocess_block(&$variables, $hook) {
  // Add a count to all the blocks in the region.
  $variables['classes_array'][] = 'count-' . $variables['block_id'];
}
// */


function jesuites_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
  
  $active = '';
  $titulo = '';

  // CANVIEM EL T�TOL DE L'OPCI� DEL MEN� DE L'ESQUERRA, PERQU� CANVII EL COLOR QUAN EST� SELECCIONADA
  if ($element['#theme'] == "menu_link__menu_esquerra") {

		$ide = substr($element['#href'],5,strlen($element['#href']));

		switch($ide) {
			case 45: 
			case 711: 	$titulo = "noticia";
					 	break;
			case 46: 
			case 712: 	$titulo = "agenda";
					 	break;
			case 47: 	$titulo = "nota_de_premsa";
					 	break;
			case 49: 	$titulo = "privat";
					 	break;
		}
	   $active = 'id="' . $titulo . '"';

  }


  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . ' ' . $active . '>' . $output . $sub_menu . "</li>\n";
}




 
function jesuites_file_icon($variables) { 
  global $theme_path;

  $file = $variables['file'];
  $icon_directory = $theme_path . "/images/";

  $mime = check_plain($file->filemime);
  $tipo = strstr($mime, '/', true);

  switch($tipo) {
  	case "image":	$mime = "Imatge";
					break;
  	case "audio":	$mime = "Audio";
					break;
  	default:		$mime = "Document";
					break;
  }

  $icon_url = file_icon_url($file, $icon_directory);
  return '<img class="file-icon" alt="" title="' . $mime . '" src="' . $icon_url . '" />';
}



/* FUNCI� PER TRADU�R LES TAXONOMIES EN ELS "EXPOSED FILTERS" D'UNA VIEW */
function jesuites_preprocess_views_exposed_form(&$vars) {
  $myviewID = 'views-exposed-form-agenda-block';

  if ($vars['form']['#id'] == $myviewID && function_exists('i18n_taxonomy_term_name')) {

    global $language;
    $langcode = $language->language;

    foreach($vars['form']['agenda']["#options"] as $term_id => $value) {
	  if ($term_id != "All") {
		  $term = taxonomy_term_load($term_id);
		  $vars['form']['agenda']["#options"][$term_id] = i18n_taxonomy_term_name($term, $langcode);
	  }
    }
    unset($vars['form']['agenda']['#printed']);
    $vars['widgets']['filter-tid']->widget = drupal_render($vars['form']['agenda']);

  }
}